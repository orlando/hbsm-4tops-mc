import os
import sys

user='orlando'
prod_tag='v2.0'

list_of_jos=['mc.MGPy8EG_NNPDF31_mh2500_wh219_hbsm4tops.py', 'mc.MGPy8EG_NNPDF31_mh2500_wh219_hbsm4tops_hrg.py']

list_of_mass_points=['400','500','600','700','800','900','1000']

for mass in list_of_mass_points:
    for job_option in list_of_jos: 
        print('Processing mass: '+mass)
        job_option = job_option.replace("500", mass)
        # For labeling the log.generate output and job command 
        process=job_option.split('_')[-1].split('.')[0]
        print('Processing: '+process)
        rm_command='rm 100xxx/100000/*.*'
        cp_command='cp '+mass+'/'+job_option+' 100xxx/100000/'
        command_to_run='pathena --trf "Gen_tf.py --ecmEnergy=13000 --jobConfig=100000 --maxEvents=10000 --outputEVNTFile %OUT.EVNT.root" --outDS user.'+user+'.gen_hbsm4tops-mg_'+prod_tag+'_'+process+'_'+mass+' --extFile '+job_option
        os.system(rm_command)
        os.system(cp_command)
        os.system(command_to_run)
        


        


